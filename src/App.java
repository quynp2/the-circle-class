import com.devcamp.basicjava.Circle;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        Circle circle1 = new Circle();
        Circle circle2 = new Circle(3.0);
        System.out.println(circle1.toString());

        System.out.println(circle2.toString());
        // lay dien tich
        double dientich1 = circle1.getArea();
        double dientich2 = circle2.getArea();
        // lay chu vi
        double chuvi1 = circle1.getCircumference();
        double chuvi2 = circle2.getCircumference();

        System.out.println("Dien tich hinh 1 la = " + dientich1 + " chu vi = " + chuvi1);

        System.out.println("Dien tich hinh 2 la = " + dientich2 + " chu vi = " + chuvi2);

    }
}
