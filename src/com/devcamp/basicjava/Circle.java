package com.devcamp.basicjava;

public class Circle {
    public double radious = 1.0;

    public Circle() {
    }

    public Circle(double radious) {
        this.radious = radious;
    }

    public double getRadious() {
        return radious;
    }

    public void setRadious(double radious) {
        this.radious = radious;
    }

    public double getArea() {
        return radious * radious * 3.14;
    }

    public double getCircumference() {
        return 2 * radious * 3.14;
    }

    @Override
    public String toString() {
        return String.format("Circle[radious=]", radious);
    }
}
